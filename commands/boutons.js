const { SlashCommandBuilder } = require('@discordjs/builders');
const { MessageActionRow, MessageButton } = require('discord.js');

module.exports = {
	data: new SlashCommandBuilder()
		.setName('boutons')
		.setDescription('tkt'),
	async execute(interaction) {
		const component = [];
        let id = 0;
		for (let i = 0; i < 5; i++) {
			const row = new MessageActionRow();
			for (let j = 0 ; j < 5; j++) {
				row.addComponents(
					new MessageButton()
						.setCustomId('' + (id))
						.setLabel('' + (i + j))
						.setStyle('PRIMARY'),
				);
                id++;
			}
            component.push(row);
		}

        

        return interaction.reply({ content:'test', components: component });
	},
};