
module.exports = {
	name: 'interactionCreate',
	async execute(interaction) {
		console.log(`${interaction.user.tag} in #${interaction.channel.name} triggered an interaction.`);
        if(interaction.isButton()){
            interaction.label = '1';
        }
        else if (!interaction.isCommand()) {return}
        else{ 
            console.log('b');
            const command = interaction.client.commands.get(interaction.commandName);

            if (!command) return;

            try {
                return command.execute(interaction);
            } catch (error) {
                console.error(error);
                return interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
            }
        } 
        
	},
};